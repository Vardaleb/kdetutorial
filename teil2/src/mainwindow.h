#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <KXmlGuiWindow>

class MainWindow : public KXmlGuiWindow
{
  Q_OBJECT
    void setupActions();
    
public:

    MainWindow ( QWidget *parent=0 );
    
private slots:
    void clear();
};

#endif // MAINWINDOW_H
