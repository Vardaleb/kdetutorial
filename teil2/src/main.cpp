#include <KAboutData>
#include <KCmdLineArgs>
#include <KApplication>
#include "mainwindow.h"

int main ( int argc, char **argv )
{
    KAboutData aboutData (
        "Tutorial",
        NULL,
        ki18n ( "Tutorial" ), // Name des Programms, erscheint im Titel des Hauptfensters
        "0.2",                                      
        ki18n ( "KDE Tutorial, Teil 2" ),
        KAboutData::License_GPL_V3,
        ki18n ( "Copyright Andreas Blochberger, 2009" ),
        ki18n ( "Ein erstes KDE Programm mit Hauptfenster" ),
        "http://argeleb.wordpress.com",
        "" ); // E-Mail Adresse zum Melden von Bugs
    KCmdLineArgs::init ( argc, argv, &aboutData );

    KApplication app;
    MainWindow* mainWindow = new MainWindow();
    mainWindow->show();

    return app.exec();
}
