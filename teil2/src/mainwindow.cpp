#include "mainwindow.h"
#include <kaction.h>
#include <klocale.h>
#include <KActionCollection>
#include <KStandardAction>
#include <KApplication>
#include <stdio.h>

MainWindow::MainWindow ( QWidget *parent ) : KXmlGuiWindow ( parent )
{
    setupActions();
}


void MainWindow::setupActions()
{
    KAction* clearAction = new KAction(this);
    clearAction->setText(i18n("Clear"));
    clearAction->setIcon(KIcon("document-new"));
    clearAction->setShortcut(Qt::CTRL+Qt::Key_W);
    connect( clearAction, SIGNAL( triggered(bool) ),
             this, SLOT( clear() ) );

    actionCollection()->addAction("clear", clearAction);

    KStandardAction::quit(kapp, SLOT(quit()), actionCollection());
    setupGUI();
}

void MainWindow::clear()
{
    printf( "clear\n" );
}
