project (tutorial)
find_package(KDE4 REQUIRED)
include (KDE4Defaults)
include_directories(${KDE4_INCLUDES})
set(tutorial_SRCS 
    src/mainwindow.cpp 
    src/main.cpp
    src/mainwindow.rc)
kde4_add_executable(tutorial ${tutorial_SRCS})
target_link_libraries(tutorial ${KDE4_KDEUI_LIBS})
install(TARGETS tutorial  ${INSTALL_TARGETS_DEFAULT_ARGS})
