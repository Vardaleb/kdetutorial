/********************************************************************************
** Form generated from reading ui file 'mainwindow.ui'
**
** Created: Sun Dec 20 17:43:40 2009
**      by: Qt User Interface Compiler version 4.5.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QFormLayout>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QProgressBar>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QTabWidget>
#include <QtGui/QTextBrowser>
#include <QtGui/QToolBar>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_2;
    QTabWidget *tabWidget;
    QWidget *tabSettings;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *checkMergeFiles;
    QLabel *label_2;
    QComboBox *comboAspect;
    QGroupBox *EingabeVerzeichnis;
    QHBoxLayout *horizontalLayout;
    QLineEdit *inputDirectory;
    QToolButton *chooseInputDirectory;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_3;
    QLineEdit *outputDirectory;
    QToolButton *chooseOutputDirectory;
    QLabel *label;
    QLineEdit *outputFile;
    QWidget *tabProgress;
    QFormLayout *formLayout;
    QProgressBar *progressFile;
    QProgressBar *progressMain;
    QLabel *file;
    QLabel *label_3;
    QLabel *txtInputDirectory;
    QLabel *txtOutput;
    QLabel *label_5;
    QSpacerItem *verticalSpacer;
    QLabel *label_4;
    QWidget *tabLog;
    QHBoxLayout *horizontalLayout_2;
    QTextBrowser *textLog;
    QDialogButtonBox *buttonBox;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(511, 422);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        gridLayout_2 = new QGridLayout(centralWidget);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setMargin(11);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabSettings = new QWidget();
        tabSettings->setObjectName(QString::fromUtf8("tabSettings"));
        verticalLayout = new QVBoxLayout(tabSettings);
        verticalLayout->setSpacing(6);
        verticalLayout->setMargin(11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_2 = new QGroupBox(tabSettings);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setMargin(11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        checkMergeFiles = new QCheckBox(groupBox_2);
        checkMergeFiles->setObjectName(QString::fromUtf8("checkMergeFiles"));

        verticalLayout_2->addWidget(checkMergeFiles);

        label_2 = new QLabel(groupBox_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_2->addWidget(label_2);

        comboAspect = new QComboBox(groupBox_2);
        comboAspect->setObjectName(QString::fromUtf8("comboAspect"));

        verticalLayout_2->addWidget(comboAspect);


        verticalLayout->addWidget(groupBox_2);

        EingabeVerzeichnis = new QGroupBox(tabSettings);
        EingabeVerzeichnis->setObjectName(QString::fromUtf8("EingabeVerzeichnis"));
        horizontalLayout = new QHBoxLayout(EingabeVerzeichnis);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setMargin(11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        inputDirectory = new QLineEdit(EingabeVerzeichnis);
        inputDirectory->setObjectName(QString::fromUtf8("inputDirectory"));

        horizontalLayout->addWidget(inputDirectory);

        chooseInputDirectory = new QToolButton(EingabeVerzeichnis);
        chooseInputDirectory->setObjectName(QString::fromUtf8("chooseInputDirectory"));

        horizontalLayout->addWidget(chooseInputDirectory);


        verticalLayout->addWidget(EingabeVerzeichnis);

        groupBox = new QGroupBox(tabSettings);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setSpacing(6);
        gridLayout->setMargin(11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        frame = new QFrame(groupBox);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_3 = new QHBoxLayout(frame);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setMargin(11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        outputDirectory = new QLineEdit(frame);
        outputDirectory->setObjectName(QString::fromUtf8("outputDirectory"));

        horizontalLayout_3->addWidget(outputDirectory);

        chooseOutputDirectory = new QToolButton(frame);
        chooseOutputDirectory->setObjectName(QString::fromUtf8("chooseOutputDirectory"));

        horizontalLayout_3->addWidget(chooseOutputDirectory);


        gridLayout->addWidget(frame, 0, 0, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        outputFile = new QLineEdit(groupBox);
        outputFile->setObjectName(QString::fromUtf8("outputFile"));

        gridLayout->addWidget(outputFile, 2, 0, 1, 1);


        verticalLayout->addWidget(groupBox);

        tabWidget->addTab(tabSettings, QString());
        tabProgress = new QWidget();
        tabProgress->setObjectName(QString::fromUtf8("tabProgress"));
        formLayout = new QFormLayout(tabProgress);
        formLayout->setSpacing(6);
        formLayout->setMargin(11);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        progressFile = new QProgressBar(tabProgress);
        progressFile->setObjectName(QString::fromUtf8("progressFile"));
        progressFile->setValue(0);

        formLayout->setWidget(6, QFormLayout::SpanningRole, progressFile);

        progressMain = new QProgressBar(tabProgress);
        progressMain->setObjectName(QString::fromUtf8("progressMain"));
        progressMain->setValue(0);

        formLayout->setWidget(4, QFormLayout::SpanningRole, progressMain);

        file = new QLabel(tabProgress);
        file->setObjectName(QString::fromUtf8("file"));

        formLayout->setWidget(5, QFormLayout::SpanningRole, file);

        label_3 = new QLabel(tabProgress);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_3);

        txtInputDirectory = new QLabel(tabProgress);
        txtInputDirectory->setObjectName(QString::fromUtf8("txtInputDirectory"));

        formLayout->setWidget(0, QFormLayout::FieldRole, txtInputDirectory);

        txtOutput = new QLabel(tabProgress);
        txtOutput->setObjectName(QString::fromUtf8("txtOutput"));

        formLayout->setWidget(1, QFormLayout::FieldRole, txtOutput);

        label_5 = new QLabel(tabProgress);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        formLayout->setWidget(3, QFormLayout::SpanningRole, label_5);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout->setItem(2, QFormLayout::LabelRole, verticalSpacer);

        label_4 = new QLabel(tabProgress);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_4);

        tabWidget->addTab(tabProgress, QString());
        tabLog = new QWidget();
        tabLog->setObjectName(QString::fromUtf8("tabLog"));
        horizontalLayout_2 = new QHBoxLayout(tabLog);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setMargin(11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        textLog = new QTextBrowser(tabLog);
        textLog->setObjectName(QString::fromUtf8("textLog"));

        horizontalLayout_2->addWidget(textLog);

        tabWidget->addTab(tabLog, QString());

        gridLayout_2->addWidget(tabWidget, 1, 0, 1, 1);

        buttonBox = new QDialogButtonBox(centralWidget);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        gridLayout_2->addWidget(buttonBox, 2, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(2);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Schnipsel", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Optionen", 0, QApplication::UnicodeUTF8));
        checkMergeFiles->setText(QApplication::translate("MainWindow", "Dateien zu einer Ausgabedatei zusammenfassen", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Aspect-Ratio:", 0, QApplication::UnicodeUTF8));
        comboAspect->clear();
        comboAspect->insertItems(0, QStringList()
         << QApplication::translate("MainWindow", "16/9", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("MainWindow", "4/3", 0, QApplication::UnicodeUTF8)
        );
        EingabeVerzeichnis->setTitle(QApplication::translate("MainWindow", "Eingabe", 0, QApplication::UnicodeUTF8));
        chooseInputDirectory->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("MainWindow", "Ausgabe", 0, QApplication::UnicodeUTF8));
        chooseOutputDirectory->setText(QApplication::translate("MainWindow", "...", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("MainWindow", "Ausgabedatei:", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tabSettings), QApplication::translate("MainWindow", "Einstellungen", 0, QApplication::UnicodeUTF8));
        file->setText(QString());
        label_3->setText(QApplication::translate("MainWindow", "Eingabe-Verzeichnis: ", 0, QApplication::UnicodeUTF8));
        txtInputDirectory->setText(QString());
        txtOutput->setText(QString());
        label_5->setText(QApplication::translate("MainWindow", "Gesamtfortschritt", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Ausgabe:", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tabProgress), QApplication::translate("MainWindow", "Fortschritt", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tabLog), QApplication::translate("MainWindow", "Protokoll", 0, QApplication::UnicodeUTF8));
        Q_UNUSED(MainWindow);
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
