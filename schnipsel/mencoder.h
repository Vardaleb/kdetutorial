#ifndef MENCODER_H
#define MENCODER_H

#include <QObject>
#include "QProcess"


class MEncoder : public QProcess
{
    Q_OBJECT

public:

    typedef enum { aspect169, aspect43 } AspectRatio;

private:

    typedef enum { none, p, o, s, colon, open } State;

    QString inputDirecory;
    QString outputDirectory;
    QString outputFile;
    bool merge;

    QStringList files;
    State state;
    QString complete;
    int percentage;
    int file;
    int startIndex;
    AspectRatio aspect;

    QString getAspectRatio();

protected:

    QString getOutputFilename();

public:

    MEncoder();
    ~MEncoder();

    void setInputDirectory( QString directory );
    void setOutputDirectory( QString directory );
    void setOutputFilename( QString fileName );
    void setMerge( bool merge );
    void setAspect( AspectRatio aspect );

    QStringList& getInputFiles();
    QString& getInputDirectory();

    void start();

signals:

    void fileProgress( int file, int percentage );
    void allJobsDone();
    void textAvailable( QString text );

public slots:

    void readStandardOutput();
    void started();
    void finished();

};

#endif // MENCODER_H
