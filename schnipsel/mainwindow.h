#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "mencoder.h"
#include "QSettings"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;
    MEncoder* mencoder;
    bool canceled;
    bool started;
    QSettings* settings;

    QString key( QString dir, QString key );

private slots:
    void on_checkMergeFiles_stateChanged(int );
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
    void on_chooseOutputDirectory_clicked();
    void on_chooseInputDirectory_clicked();
    void onFileProgress(int file,int percentage);
    void onFinished();
    void onTextAvailable( QString text );
};

#endif // MAINWINDOW_H
