/****************************************************************************
** Meta object code from reading C++ file 'mencoder.h'
**
** Created: Sun Dec 20 17:28:21 2009
**      by: The Qt Meta Object Compiler version 61 (Qt 4.5.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mencoder.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mencoder.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 61
#error "This file was generated using the moc from 4.5.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MEncoder[] = {

 // content:
       2,       // revision
       0,       // classname
       0,    0, // classinfo
       6,   12, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors

 // signals: signature, parameters, type, tag, flags
      26,   10,    9,    9, 0x05,
      48,    9,    9,    9, 0x05,
      67,   62,    9,    9, 0x05,

 // slots: signature, parameters, type, tag, flags
      90,    9,    9,    9, 0x0a,
     111,    9,    9,    9, 0x0a,
     121,    9,    9,    9, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MEncoder[] = {
    "MEncoder\0\0file,percentage\0"
    "fileProgress(int,int)\0allJobsDone()\0"
    "text\0textAvailable(QString)\0"
    "readStandardOutput()\0started()\0"
    "finished()\0"
};

const QMetaObject MEncoder::staticMetaObject = {
    { &QProcess::staticMetaObject, qt_meta_stringdata_MEncoder,
      qt_meta_data_MEncoder, 0 }
};

const QMetaObject *MEncoder::metaObject() const
{
    return &staticMetaObject;
}

void *MEncoder::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MEncoder))
        return static_cast<void*>(const_cast< MEncoder*>(this));
    return QProcess::qt_metacast(_clname);
}

int MEncoder::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QProcess::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: fileProgress((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 1: allJobsDone(); break;
        case 2: textAvailable((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 3: readStandardOutput(); break;
        case 4: started(); break;
        case 5: finished(); break;
        default: ;
        }
        _id -= 6;
    }
    return _id;
}

// SIGNAL 0
void MEncoder::fileProgress(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MEncoder::allJobsDone()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void MEncoder::textAvailable(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
