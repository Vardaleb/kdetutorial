#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDir"
#include "QMessageBox"
#include "QFileDialog"
#include "QPushButton"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(0);

    started = false;
    canceled = false;
    mencoder = new MEncoder();
    connect(mencoder,SIGNAL(fileProgress(int,int)), this, SLOT(onFileProgress(int,int)));
    connect(mencoder, SIGNAL(allJobsDone()), this, SLOT(onFinished()));
    connect(mencoder, SIGNAL(textAvailable(QString)), this, SLOT(onTextAvailable(QString)));

    settings = new QSettings("argeleb.wordpress.com", "Schnipsel",this);

    QString dirSetting = QDir::current().absolutePath();
    ui->inputDirectory->setText(settings->value( key( dirSetting, "inputDirectory"), "in").toString());
    ui->outputDirectory->setText(settings->value( key( dirSetting, "outputDirectory"), "out").toString());
    ui->outputFile->setText(settings->value( key( dirSetting, "outputFile"), "complete").toString());
    ui->checkMergeFiles->setChecked(settings->value( key( dirSetting, "merge"), true).toBool());
    ui->comboAspect->setCurrentIndex(settings->value( key( dirSetting, "aspect"), 0).toInt());

    ui->outputFile->setEnabled(ui->checkMergeFiles->isChecked());

    int _x = settings->value( key( dirSetting, "window.x"), x()).toInt();
    int _y = settings->value( key( dirSetting, "window.y"), y()).toInt();
    int _width = settings->value( key( dirSetting, "window.width"), width()).toInt();
    int _height = settings->value( key( dirSetting, "window.height"), height()).toInt();
    move( _x, _y );
    resize(_width, _height);
}

MainWindow::~MainWindow()
{
    QString dirSetting = QDir::current().absolutePath();
    settings->setValue(key( dirSetting, "inputDirectory"), QVariant(ui->inputDirectory->text()));
    settings->setValue(key( dirSetting, "outputDirectory"), QVariant(ui->outputDirectory->text()));
    settings->setValue(key( dirSetting, "outputFile"), QVariant(ui->outputFile->text()));
    settings->setValue(key( dirSetting, "merge"), QVariant(ui->checkMergeFiles->isChecked()));
    settings->setValue(key( dirSetting, "aspect"), QVariant(ui->comboAspect->currentIndex()));

    settings->setValue(key(dirSetting,"window.x"), QVariant(x()));
    settings->setValue(key(dirSetting,"window.y"), QVariant(x()));
    settings->setValue(key(dirSetting,"window.width"), QVariant(width()));
    settings->setValue(key(dirSetting,"window.height"), QVariant(height()));

    delete ui;
    delete mencoder;
    delete settings;
}

QString MainWindow::key( QString dir, QString key )
{
    return dir + "/" + key;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow :: onFileProgress(int file,int percentage)
{
    QStringList files = mencoder->getInputFiles();
    int mainPercentage = int(((double)file / (double)files.size()) * 100.0);
    ui->progressMain->setValue( mainPercentage );
    if( file < files.size() )
        ui->file->setText( mencoder->getInputDirectory() + "/" + files.at(file));
    ui->progressFile->setValue(percentage);
}

void MainWindow :: onFinished()
{
    if( !canceled )
        QMessageBox::information(this,"Schnipsel","Encoding done!");
    close();
}

void MainWindow::on_chooseInputDirectory_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this,"Eingabe-Verzeichnis",".");
    if( !dir.isEmpty())
        ui->inputDirectory->setText(dir);
}

void MainWindow::on_chooseOutputDirectory_clicked()
{
    QString dir = QFileDialog::getExistingDirectory(this,"Ausgabe-Verzeichnis",".");
    if( !dir.isEmpty())
        ui->outputDirectory->setText(dir);
}

void MainWindow::on_buttonBox_accepted()
{
    started = true;
    ui->tabWidget->setCurrentIndex(1);
    ui->textLog->setText("");

    mencoder->setMerge(ui->checkMergeFiles->isChecked());

    QString setting = ui->inputDirectory->text();
    if( setting.isEmpty())
        setting = "in";
    mencoder->setInputDirectory( setting );
    ui->txtInputDirectory->setText( setting );

    setting = ui->outputDirectory->text();
    if( setting.isEmpty())
        setting = "out";
    mencoder->setOutputDirectory( setting );
    ui->txtOutput->setText( setting );

    setting = ui->outputFile->text();
    if( setting.isEmpty())
        setting = "complete";
    if( ui->comboAspect->currentIndex() == 0 )
    {
        mencoder->setAspect(MEncoder::aspect169);
        setting += "-16_9";
    }
    else
    {
        mencoder->setAspect(MEncoder::aspect43);
        setting += "-4_3";
    }

    setting += ".mpg";
    mencoder->setOutputFilename( setting );

    if( ui->checkMergeFiles->isChecked() )
        ui->txtOutput->setText( ui->txtOutput->text() + "/" + setting );
    else
        ui->txtOutput->setText( ui->txtOutput->text() );

    mencoder->start();

    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled( false );
}

void MainWindow::on_buttonBox_rejected()
{
    if( started )
    {
        if( QMessageBox::question(this,"Bestätigung",
            "Wollen Sie den Konvertierungsvorgang wirklich abbrechen?", QDialogButtonBox::Yes, QDialogButtonBox::No) == QDialogButtonBox::Yes)
        {
            canceled = true;
            close();
        }
    }
    else
        close();
}

void MainWindow::on_checkMergeFiles_stateChanged(int )
{
    ui->outputFile->setEnabled(ui->checkMergeFiles->isChecked());
}

void MainWindow::onTextAvailable( QString text )
{
    ui->textLog->append(text);
}
